# CICD Pipeline for a React App 

## Step 1
 I have created a .gitlab-yml file where two stages are being stated one is build and another is deploy.
 After build the artifacts will be deployed on Netlify. 
## Step 2 
 I have integrated Netlify with gitlab. 
## Step 3
 I have created a void site on Netlify and get the site ID. Then deploy the artifacts on that site using Netlify site ID and Netlify access token.  
